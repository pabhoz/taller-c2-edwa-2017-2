<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AlienFactory
 *
 * @author pabhoz
 */
class AlienFactory implements IAlienFactory{
    
    public function getAlien($nombre,$edad,$especie,$planeta) : Alien{
        return new Alien($nombre, $edad, $especie, $planeta);
    }

    public function getJupiterAlien($nombre, $edad, $especie): \JupiterAlien {
        return new JupiterAlien($nombre, $edad, $especie);
    }

    public function getMarsAlien($nombre, $edad, $especie): \MarsAlien {
        return new MarsAlien($nombre, $edad, $especie);
    }

    public function getMoonAlien($nombre, $edad, $especie): \MoonAlien {
        return new MoonAlien($nombre, $edad, $especie);
    }

    public function getPlutoAlien($nombre, $edad, $especie): \PlutoAlien {
        return new PlutoAlien($nombre, $edad, $especie);
    }

    public function getVenusAlien($nombre, $edad, $especie): \VenusAlien {
        return new VenusAlien($nombre, $edad, $especie);
    }

    public function getSaturnAlien($nombre, $edad, $especie): \SaturnAlien {
       return new SaturnAlien($nombre, $edad, $especie); 
    }

}
